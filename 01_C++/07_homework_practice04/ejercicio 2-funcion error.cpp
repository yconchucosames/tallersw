
/**
 * @file Condicionales.cpp
 Ejercicio 2:
A un  trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a 1000,
 por encima de 1000 hasta 2000 el 5% del adicional, y por encima de 2000 el 3% del adicional.
  Calcular el descuento y sueldo neto que recibe el trabajador dado un sueldo,
 * @author Yhen conchucos(yconchucosames@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 4.02.2022
 * 
 * && y
 * || o 
 * == igual
 * != diferente
 *
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

float sueldoBruto =0.0;
float descuento=0.0;
float sueldo=0.0;

	typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
	} Result;



/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	CollectData();
		
	ShowResults();
	

		
	if(ShowResults() ==false){
		
		cout<<"error en ShowResult\r\n";
		
	}	
		
		
	
	}
	
	
	

//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Ingrese su sueldo bruto: ";
	cin>>sueldoBruto;
	
	if (cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		sueldoBruto = 0;
		cout<<"dato ingresado es incorrecto\r\n";
		return Error;
	}
}



//=====================================================================================================
	
bool ShowResults(){
	cout<<"============Show result============\r\n";
		
		
		
		if(sueldoBruto<=1000 ){
			
			descuento=10*sueldoBruto/100;
			sueldo=sueldoBruto-descuento;
			cout<<"el sueldo es \r\n"<<sueldo<<endl;
			cout<<"el descuento es \r\n"<<descuento;
			
			
			
			
		}else if(sueldoBruto>1000 && sueldoBruto<=2000){
			
			float diferencia=0;
			
			 diferencia=sueldoBruto-1000 ;
			descuento= 100+5*diferencia/100;
			sueldo=sueldoBruto-descuento;
			cout<<"el sueldo es \r\n"<<sueldo<<endl;
			cout<<"el descuento es \r\n"<<descuento;
			
			
		}else {
			
			descuento =150+(sueldoBruto-2000)*3/100;
			sueldo=sueldoBruto-descuento;
			cout<<"el sueldo es \r\n"<<sueldo<<endl;
			cout<<"el descuento es \r\n"<<descuento;
		}
	        
			}
			
			
			
			





