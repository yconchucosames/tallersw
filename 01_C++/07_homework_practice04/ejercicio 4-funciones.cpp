/**
 * @file CodeReviewExercise04.cpp
 .- Dado un tiempo en minutos, calcular los d�as, horas y minutos que    le corresponden
 * @author Yhen Conchucos Ames(yconchucos@gmail.com)
 * @brief exercise 04
 * @version 1.0
 * @date 4.03.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int tiempo=0;
int minutos=0;
int horas=0;
int dias=0;
int desg=0;
int x=0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
bool Calculate();
bool ShowResult();
int DesglosarTiempo(int tiempo );   //ordena de mayor a menor los numeros ingresados

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	CollectData();
	Calculate();
	ShowResult();
	
}
//=====================================================================================================

Result CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese el tiempo: "<<endl;
	cin>>tiempo;
	if(cin.fail()==true){
		cin.clear();
		tiempo=0;
		cout<<"falla al ingresar el tiempo\n\r";
		
		return Error;
	}
	
}
//=====================================================================================================

bool Calculate(){
	desg=DesglosarTiempo( tiempo );
	
}
//=====================================================================================================
	bool ShowResult(){
		
		switch(desg){
			
			case 1:
				cout<<dias<<" "<<"dias"<<"  "<<horas<<" "<<"horas"<<" "<<minutos<<" "<<"minutos";
		break;
			default:
				cout<<"falla al mostrar resultados\r\n";
				break;
			
		}
		
	
	
}
//=====================================================================================================

	
	int DesglosarTiempo(int tiempo ){
		
		dias=tiempo/1440;
		x=tiempo%1440;
		horas=x/60;
		minutos=x%60;
		
		
		
	 return desg ;
		
	}
		
	
//=====================================================================================================


