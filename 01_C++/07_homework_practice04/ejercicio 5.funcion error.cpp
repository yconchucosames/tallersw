/**
 * @file CodeReviewExercise05.cpp
 .- Dados tres datos enteros positivos, que representen las longitudes de un posible triangulo, 
 determine si los datos corresponden a un triangulo. En caso afirmativo, 
 escriba si el triangulo es equil�tero, is�sceles o escaleno. Calcule adem�s su �rea.
 * @author Yhen Conchucos Ames(yconchucos@gmail.com)
 * @brief exercise 05
 * @version 1.0
 * @date 4.03.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int numero1=0;
int numero2=0;
int numero3=0;
int triangulo=0;

typedef enum{
Success,
Error,
} Result;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
bool Calculate();
float Triangulo(int numero1,int numero2,int numero3 );   


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	CollectData();
	Calculate();
	
		
}
//=====================================================================================================

Result CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese primer numero "<<endl;
	cin>>numero1;
	
	if(cin.fail()==true){
		cin.clear();
		numero1=0;
		cout<<"primer numero ingresado de manera incorrecta\r\n";
		return Error;
	}
	cout<<"Ingrese segundo numero "<<endl;
	cin>>numero2;
	
	if(cin.fail()==true){
		cin.clear();
		numero2=0;
		cout<<"segundo numero ingresado de manera incorrecta\r\n";
		return Error;
		
	}
	cout<<"Ingrese tercer numero "<<endl;
	cin>>numero3;
	
	if(cin.fail()==true){
		cin.clear();
		numero3=0;
		cout<<"tercer numero ingresado de manera incorrecta\r\n";
		return Error;
		
	}
}
//=====================================================================================================

bool Calculate(){
	Triangulo( numero1, numero2, numero3 );
	
}
//=====================================================================================================
	
//=====================================================================================================

	
	float Triangulo(int numero1,int numero2,int numero3 ){
		
	
				if(numero1+numero2>numero3 && numero1+numero3>numero2 && numero2+numero3>numero1){
			float area;
			float s;
			
			s=(numero1+numero2+numero3)/2;
			
			area=sqrt(s*(s-numero1)*(s-numero2)*(s-numero3));
			
			cout<<"el area del triangulo es :"<<area<<endl;
			
			if(numero1==numero2  && numero1==numero3){
				
				cout<<"es un triangulo equilatero"<<endl;
				
			}else if(numero1==numero2  || numero1==numero3){
				
				cout<<"es un  triangulo  isosceles"<<endl;
			}else if(numero1!=numero2  && numero1!=numero3)
			{
				cout<<"es un triangulo escaleno"<<endl;
			}
			
			
		} 
		
		else{
			cout<<"no existe tal triangulo"<<endl;
		}
		
	
		
		}
		
		
	
		
	
//=====================================================================================================


