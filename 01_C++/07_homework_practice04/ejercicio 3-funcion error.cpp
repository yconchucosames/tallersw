/**
 * @file CodeReviewExercise03.cpp
 Ordene de mayor a menor 3 n�meros  ingresados por teclado
 * @author Yhen Conchucos Ames(yconchucos@gmail.com)
 * @brief exercise 02
 * @version 1.0
 * @date 13.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int numero1=0;
int numero2=0;
int numero3=0;
int ordenar=0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
bool Calculate();
int Ordenamiento(int numero1, int numero2,int numero3 );   //ordena de mayor a menor los numeros ingresados

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	if(Run()==false)
{cout<<"Error";
	
	}else{
		return 0;	
	}	

}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	CollectData();
	if (Calculate() == false ){
			cout<<"Codigo ingresa es invalido, vuelva a ingresar el codigo\r\n";
					}
	
}
//=====================================================================================================

Result CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese primer numero: "<<endl;
	cin>>numero1;
	
	if(cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		numero1=0;
		cout<<"error al digitar el primer numero";
		return Error;
	}
	cout<<"Ingrese segundo numero: "<<endl;
	cin>>numero2;
	
	if(cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		numero2=0;
		cout<<"error al digitar el segundo numero";
		return Error;
	}
	cout<<"Ingrese tercer numero: "<<endl;
	cin>>numero3;
	
	if(cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		numero3=0;
		cout<<"error al digitar el tercer numero";
		return Error;
	}
}
//=====================================================================================================

bool Calculate(){
	ordenar=Ordenamiento( numero1,  numero2, numero3 );
	
	
}
//=====================================================================================================


int Ordenamiento(int numero1, int numero2,int numero3 ){
	
	if(numero1>numero2 &&numero1>numero3 ){
		
	if(numero2>numero3){if(true){
		
		
	
		
		cout<<"los numeros ordenados de mayor a menor son :"<<endl;
		cout<<numero1<<" , "<<numero2<<" , "<<numero3<<endl;
	}else{
		cout<<"los numeros ordenados de mayor a menor son :"<<endl;
		cout<<numero1<<" , "<<numero3<<" , "<<numero2<<endl;
	}

	} 
	
	if(numero2>numero1 &&numero2>numero3 ){
		if(numero1>numero3){
			
		cout<<"los numeros ordenados de mayor a menor son :"<<endl;	
	    cout<<numero2<<" , "<<numero1<<" , "<<numero3<<endl;
	    
		}else{
			
			cout<<"los numeros ordenados de mayor a menor son :"<<endl;
			cout<<numero2<<" , "<<numero3<<" , "<<numero1<<endl;
		}
	
	
	
	} 
	
	 if(numero3>numero1 &&numero3>numero2){
	 	if(numero1>numero2){
	 		
	 		cout<<"los numeros ordenados de mayor a menor son :"<<endl;
	 		cout<<numero3<<" , "<<numero1<<" , "<<numero2<<endl;
		 }else{
		 	
		 	cout<<"los numeros ordenados de mayor a menor son :"<<endl;
		 	cout<<numero3<<" , "<<numero2<<" , "<<numero1<<endl
			 ;
		 }
	


		}
			
		}	
	
//=====================================================================================================


