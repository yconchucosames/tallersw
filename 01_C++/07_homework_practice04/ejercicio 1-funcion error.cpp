/**-
 * @file Condicionales.cpp
 Ejercicio 1:funcion error
 Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas y el pago por hora.
Si el total de horas trabajadas es mayor a 40 la diferencia se considera como horas extras y se paga un 50% mas que una hora normal.
Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en caso contrario el descuento es 0.

 * @author Yhen conchucos(yconchucosames@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 19.02.2022
 * 
 * && y
 * || o 
 * == igual
 * != diferente
 *
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

int totalHorasTrabajadas =0;
int pagoHora=0;
float sueldo=0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

bool Run();
Result CollectData();
bool calculate();
bool VerificarResultado();
bool  PagoSemanal(int totalHorasTrabajadas, int pagoHora);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while(true){
	if(CollectData()==Error){
		cout<<"error al ingresar los datos de horas trabajadas y pago por hora\r\n";
		continue;
	}	
	if(calculate()==Error){
		cout<<"error al realizar los calculos\r\n";
		continue;	
		
	}
	
	if(	VerificarResultado()==Error){
		cout<<"error al verificar los resultados\r\n";
		continue;	
	break;	
	}
	
	
	return true;
}

}
//=====================================================================================================

Result CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Ingrese las horas trabajadas: ";
	cin>>totalHorasTrabajadas;
	
	//preguntar si hubo un error al guardar el dato en una variable int
	if(cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		totalHorasTrabajadas=0;
		cout<<"el dato ingresado es invalido \r\n";
		return Error;
	}
	
	cout<<"pago por horas trabajadas: ";
	cin>>pagoHora;
	
	if(cin.fail()==true){
		cin.clear();
		cin.ignore(1000,'\n');
		pagoHora=0;
		cout<<"el dato ingresado es invalido \r\n";
		return Error;
	}
	

	return Success;
}
//=====================================================================================================

bool calculate(){
	
	sueldo=PagoSemanal( totalHorasTrabajadas,  pagoHora);
	
}
//=====================================================================================================

	bool VerificarResultado(){
		
	float sueldoParcial=0.0;
	
	if(sueldo>500){
	sueldoParcial=sueldo*90/100;
		
	cout<<"el sueldo a ganar es "<<sueldoParcial;
		
		
	}else
	
	
	{
		
	cout<<"el sueldo a ganar es "<<sueldo;	
		
	}

}




bool PagoSemanal( int totalHorasTrabajadas,int pagoHora){
	
	float total_parcial=0.0;
	float horasExtras=0.0;
	float pagoParcial=0.0;
 if(totalHorasTrabajadas>40){
 	
 	horasExtras=totalHorasTrabajadas-40;
 	pagoParcial=(50*horasExtras*pagoHora)/100;
	total_parcial=pagoParcial+totalHorasTrabajadas*pagoHora;
	}else{
		
	total_parcial=totalHorasTrabajadas*pagoHora;
	
	} 
	return total_parcial;
}










