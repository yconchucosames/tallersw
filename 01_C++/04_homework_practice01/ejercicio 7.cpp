#include <iostream>
#include <math.h>

using namespace std;

int main(){
	
	double masa1,masa2,distancia,fuerza;
	
	#define double G=6,673*pow(10,-8);
	
	cout<<"ingrese la masa 1 :";
	cin>>masa1;

	cout<<"\n\ringrese la masa 2 :";
	cin>>masa2;
	
	cout<<"ingrese la distancia :";
	cin>>distancia;
	
	fuerza=G*masa1*masa2/pow(distancia,2);
	
	cout<<"la fuerza de atraccion entre las 2 masas es :"<<fuerza;
	
	
	return 0;
}
