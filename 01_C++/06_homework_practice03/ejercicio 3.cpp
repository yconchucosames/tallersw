/**
 * @file CodeReviewExercise03.cpp
 Ordene de mayor a menor 3 n�meros  ingresados por teclado
 * @author Yhen Conchucos Ames(yconchucos@gmail.com)
 * @brief exercise 02
 * @version 1.0
 * @date 13.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int numero1=0;
int numero2=0;
int numero3=0;
int ordenar=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
int Ordenamiento(int numero1, int numero2,int numero3 );   //ordena de mayor a menor los numeros ingresados

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese primer numero: "<<endl;
	cin>>numero1;
	cout<<"Ingrese segundo numero: "<<endl;
	cin>>numero2;
	cout<<"Ingrese tercer numero: "<<endl;
	cin>>numero3;
}
//=====================================================================================================

void Calculate(){
	ordenar=Ordenamiento( numero1,  numero2, numero3 );
	
}
//=====================================================================================================


int Ordenamiento(int numero1, int numero2,int numero3 ){
	
	if(numero1>numero2 &&numero1>numero3 ){
		
	if(numero2>numero3){
		
		cout<<"los numeros ordenados de mayor a menor son :"<<endl;
		cout<<numero1<<" , "<<numero2<<" , "<<numero3<<endl;
	}else{
		cout<<"los numeros ordenados de mayor a menor son :"<<endl;
		cout<<numero1<<" , "<<numero3<<" , "<<numero2<<endl;
	}

	} 
	
	if(numero2>numero1 &&numero2>numero3 ){
		if(numero1>numero3){
			
		cout<<"los numeros ordenados de mayor a menor son :"<<endl;	
	    cout<<numero2<<" , "<<numero1<<" , "<<numero3<<endl;
	    
		}else{
			
			cout<<"los numeros ordenados de mayor a menor son :"<<endl;
			cout<<numero2<<" , "<<numero3<<" , "<<numero1<<endl;
		}
	
	
	
	} 
	
	 if(numero3>numero1 &&numero3>numero2){
	 	if(numero1>numero2){
	 		
	 		cout<<"los numeros ordenados de mayor a menor son :"<<endl;
	 		cout<<numero3<<" , "<<numero1<<" , "<<numero2<<endl;
		 }else{
		 	
		 	cout<<"los numeros ordenados de mayor a menor son :"<<endl;
		 	cout<<numero3<<" , "<<numero2<<" , "<<numero1<<endl
			 ;
		 }
	


		}
			
		}	
	
//=====================================================================================================


