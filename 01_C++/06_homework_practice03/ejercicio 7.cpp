/**
 * @file CodeReviewExercise07.cpp
 Una compa��a de alquiler de autos emite la factura de sus clientes teniendo en cuenta la distancia recorrida,
  si la distancia no rebasa los 300 km., se cobra una tarifa fija de S/.250,
   si la distancia recorrida es mayor a 300 km. y hasta 1000 km.
    Se cobra la tarifa fija m�s el exceso de kil�metros a raz�n de S/.30 por km.
	 y si la distancia recorrida es mayor a 1000 km.,  la compa��a cobra la tarifa fija m�s los kms. recorridos entre 300 y 1000
	  a raz�n de S/. 30, y S/.20 para las distancias mayores de 1000 km. Calcular el monto que pagar� un cliente.


 * @author Yhen Conchucos Ames(yconchucos@gmail.com)
 * @brief exercise 07
 * @version 1.0
 * @date 16.02.2022
 */

 

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int tarifa=0;
int distancia=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
int Monto(int distancia );   


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	
		
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese los kilometros recorridos "<<endl;
	cin>>distancia;

}
//=====================================================================================================

void Calculate(){
	  Monto( distancia );
	
}
//=====================================================================================================

	
	int Monto( int distancia ){
		int diferencia=0;
		
	if(distancia<=300){
		
		cout<<"el pago por el alquiler es S/.250"<<endl;
		
	}else if (distancia>300 && distancia<=1000){
		
	diferencia=distancia-300;
	tarifa=250 + diferencia*30;
	
	cout<<"el pago por el alquiler es "<<tarifa<<endl;
	}else if(distancia>1000){
		
		tarifa=250 + 700*30 + (distancia-1000)*20;
		
		cout<<"el pago por el alquiler es "<<tarifa<<endl;
	}
	}
		

	
		
	
//=====================================================================================================


