/**
 * @file CodeReviewExercise05.cpp
 .- Dados tres datos enteros positivos, que representen las longitudes de un posible triangulo, 
 determine si los datos corresponden a un triangulo. En caso afirmativo, 
 escriba si el triangulo es equil�tero, is�sceles o escaleno. Calcule adem�s su �rea.
 * @author Yhen Conchucos Ames(yconchucos@gmail.com)
 * @brief exercise 05
 * @version 1.0
 * @date 13.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int numero1=0;
int numero2=0;
int numero3=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
float Triangulo(int numero1,int numero2,int numero3 );   


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	
		
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese primer numero "<<endl;
	cin>>numero1;
	cout<<"Ingrese segundo numero "<<endl;
	cin>>numero2;
	cout<<"Ingrese tercer numero "<<endl;
	cin>>numero3;
	
}
//=====================================================================================================

void Calculate(){
	Triangulo( numero1, numero2, numero3 );
	
}
//=====================================================================================================
	
//=====================================================================================================

	
	float Triangulo(int numero1,int numero2,int numero3 ){
		
		if(numero1+numero2>numero3 && numero1+numero3>numero2 && numero2+numero3>numero1){
			float area;
			float s;
			
			s=(numero1+numero2+numero3)/2;
			
			area=sqrt(s*(s-numero1)*(s-numero2)*(s-numero3));
			
			cout<<"el area del triangulo es :"<<area<<endl;
			
			if(numero1==numero2  && numero1==numero3){
				
				cout<<"es un triangulo equilatero"<<endl;
				
			}else if(numero1==numero2  || numero1==numero3){
				
				cout<<"es un  triangulo  isosceles"<<endl;
			}else if(numero1!=numero2  && numero1!=numero3)
			{
				cout<<"es un triangulo escaleno"<<endl;
			}
			
			
		} 
		
		else{
			cout<<"no existe tal triangulo"<<endl;
		}
		
	}
		
	
//=====================================================================================================


