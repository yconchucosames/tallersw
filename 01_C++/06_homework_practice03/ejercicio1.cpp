/**
 * @file Condicionales.cpp
 Ejercicio 1:
 Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas y el pago por hora.
Si el total de horas trabajadas es mayor a 40 la diferencia se considera como horas extras y se paga un 50% mas que una hora normal.
Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en caso contrario el descuento es 0.

 * @author Yhen conchucos(yconchucosames@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 4.02.2022
 * 
 * && y
 * || o 
 * == igual
 * != diferente
 *
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

float totalHorasTrabajadas =0;
float pagoHora=0;
float sueldo=0;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

void Run();
void CollectData();
void calculate();
void VerificarResultado();
float  PagoSemanal(float totalHorasTrabajadas, float pagoHora);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	calculate();
	VerificarResultado();
}
//=====================================================================================================

void CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Ingrese las horas trabajadas: ";
	cin>>totalHorasTrabajadas;
	
	cout<<"pago por horas trabajadas: ";
	cin>>pagoHora;

	
}
//=====================================================================================================

void calculate(){
	
	sueldo=PagoSemanal( totalHorasTrabajadas,  pagoHora);
	
}
//=====================================================================================================

	void VerificarResultado(){
		
	float sueldoParcial=0.0;
	
	if(sueldo>500){
	sueldoParcial=sueldo*90/100;
		
	cout<<"el sueldo a ganar es "<<sueldoParcial;
		
		
	}else{
		
	cout<<"el sueldo a ganar es "<<sueldo;	
		
	}

}




float PagoSemanal( float totalHorasTrabajadas,float pagoHora){
	
	float total_parcial=0.0;
	float horasExtras=0.0;
	float pagoParcial=0.0;
 if(totalHorasTrabajadas>40){
 	
 	horasExtras=totalHorasTrabajadas-40;
 	pagoParcial=(50*horasExtras*pagoHora)/100;
	total_parcial=pagoParcial+totalHorasTrabajadas*pagoHora;
	}else{
		
	total_parcial=totalHorasTrabajadas*pagoHora;
	
	} 
	return total_parcial;
}









