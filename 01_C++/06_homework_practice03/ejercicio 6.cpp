/**
 * @file CodeReviewExercise06.cpp
 Dada la hora del d�a en horas, minutos y segundos encuentre la hora del siguiente segundo.


 * @author Yhen Conchucos Ames(yconchucos@gmail.com)
 * @brief exercise 06
 * @version 1.0
 * @date 14.02.2022
 */

 * 

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int segundostotales=0;
int x=0.0;
int hora=0.0;
int minutos=0.0;
int segundos;

typedef enum{
	Error,
	
}Result;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
bool Calculate();
int TiempoDia(int segundostotales );   


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

bool Run(){
	CollectData();
	Calculate();
	
		
}
//=====================================================================================================

Result CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese la cantidad de segundos "<<endl;
	cin>>segundostotales;
	
	if(cin.fail()==true){
		cin.clear();
		segundostotales=0;
		
		cout<<"tiempo en segundos ingresado de manera incorrecta\r\n";
		
		return Error;
	}

}
//=====================================================================================================

bool Calculate(){
	  TiempoDia( segundostotales );
	
}
//=====================================================================================================

	
	int TiempoDia(int segundostotales ){
		
		if(segundostotales>=3600){ 
		hora=segundostotales/3600;
		x=segundostotales%3600;
		cout<<hora<<" horas"<<endl;
		
		if(x>=60){
		//x=segundostotales%3600;	
		minutos=x/60;
		segundos=x%60;	
		
		cout<<minutos<<" minutos"<<endl;
		cout<<segundos<<" segundos"<<endl;
		
		} else {
			cout<<"  0 minutos ";
			cout<<x<<" segundos";
		}	
			
			
		} else{
			
			cout<<" 0 horas "<<endl;
			minutos=segundostotales/60;
			segundos=segundostotales%60;
			
			cout<<minutos<<" minutos"<<endl;
			cout<<segundos<<" segundos"<<endl;
		}
		
		
	}
		
	
//=====================================================================================================


