/**
 * @file CodeReviewExercise17.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 17:
 El �rea de una elipse se obtiene con la f�rmula ?ab , donde a es el radio menor de la elipse y b es el radio mayor, 
 y su per�metro se obtiene con la f�rmula ?[4(a+b)2]0.5. 
 Realice un programa en C ++ utilizando estas f�rmulas y calcule el �rea y el per�metro de una elipse que tiene un radio menor de 2.5 cm
  y un radio mayor de 6.4 cm.
 * @version 1.0
 * @date 26.01.2022
 * 
 */
 


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float radio_menor=0.0;            
	float  radio_mayor=0.0;   
	float perimetro=0.0;
	float area=0.0;
	
	

 	
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationPerimetro(float radio_menor,float radio_mayor );
 	float CalculationArea(float radio_menor,float radio_mayor );
 
 	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar radio menor :\r\n";
 	cin>>radio_menor;
 	cout<<"ingresar radio mayor :\r\n";
 	cin>>radio_mayor;
 	

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	perimetro=CalculationPerimetro( radio_menor, radio_mayor );
 	area= CalculationArea( radio_menor, radio_mayor );
 
	
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"El perimetro de la elipse es   : "<<perimetro<<"\r\n";
 	cout<<"El area  de la elipse es   : "<<area<<"\r\n";
 	
 
	 }
//=====================================================================================================

	float CalculationPerimetro(float radio_menor,float radio_mayor ){
		
		
		
		
			return M_PI*4*(radio_menor+radio_mayor);
	}
 
	


//=====================================================================================================


float CalculationArea(float radio_menor,float radio_mayor ){
	
	
	
	
	return M_PI*radio_menor*radio_mayor;
}
