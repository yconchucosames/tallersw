/**
 * @file CodeReviewExercise18.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 18:
 Si coloca una escalera de 3 metros a un �ngulo de 85 grados al lado de un edificio,
  la altura en la cual la escalera toca el edificio se puede calcular como
   altura=3 * seno 85�. Calcule esta altura con una calculadora y 
   luego escriba un programa en C que obtenga y visualice el valor de la altura. 
 * @version 1.0
 * @date 27.01.2022
 * 
 */
 


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <cmath>



using namespace std;


/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float altura=0.0;            
	float escalera=0.0;   
	float grados=0.0;              //angulo de inclinacion de la escalera
	float pi=3.14159265;
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationAltura(float grados,float escalera );
 
 
 	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar tama�o de la escalera :\r\n";
 	cin>>escalera;
 	cout<<"ingresar angulo de inclinacin de la escalera :\r\n";
 	cin>>grados;
 
 	

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	altura=CalculationAltura( grados, escalera );
	
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"la altura en la  cual la escalera toca el edificio es   : "<<altura<<"\r\n";
 	
	 }
//=====================================================================================================

		float CalculationAltura(float grados,float escalera ){
		
		
			
			return  escalera*sin(grados*pi/180) ;
	}
 
	

