/**
 * @file CodeReviewExercise10.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 10:
 		Un millonario exc�ntrico ten�a tres hijos: Carlos, Jos� y Marta. Al morir dej� el siguiente legado:
		  A Jos� le dej� 4/3 de lo que le dej� a Carlos. A Carlos le dej� 1/3 de su fortuna. 
		  A Marta le dejo la mitad de lo que le dej� a Jos�.
		   Preparar un algoritmo para darle la suma a repartir e imprima cuanto le toc� a cada uno.
 * @version 1.0
 * @date 20.01.2022
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float   carlos=0.0;           
 	float   jose=0.0;               
 	float   marta = 0;			
	float fortunatotal=0.0;	
	
 	
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationFortunaCarlos(float fortunatotal );
	float CalculationFortunaJose(float fortunatotal );
	float CalculationFortunaMarta(float fortunatotal );
	
	

/***********************************************
 *  												MAIN
 ***********************************************/

 int main(){
 	
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Cual es la fortuna total del padre :\r\n";
 	cin>>fortunatotal;
	
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
	carlos =CalculationFortunaCarlos( fortunatotal );
	jose =CalculationFortunaJose( fortunatotal );
	marta =CalculationFortunaMarta( fortunatotal );


}
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"lo que recibe cada uno de los hijos se detalla a cotinuacion :\r\n";
 	cout<<"Carlos:S/."<<carlos<<"\r\n";
 	cout<<"Jose :S/."<<jose<<"\r\n";
 	cout<<"Marta :S/."<<marta;
 	
	 }
//=====================================================================================================

float CalculationFortunaCarlos(float fortunatotal){
	
	return      fortunatotal/3;
}
//=====================================================================================================

float CalculationFortunaJose(float fortunatotal){
	
	return      4*fortunatotal/9;
}

//=====================================================================================================

float CalculationFortunaMarta(float fortunatotal){
	
	return 2*fortunatotal/9;
}


