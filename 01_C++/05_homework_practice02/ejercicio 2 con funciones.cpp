/*
 * @file CodeReview.cpp
 * @author Yhen Conchucos(yconchucosames@gmail.com)
 * @brief Excercise 02:
 	Un maestro desea saber que porcentaje de hombres y que porcentaje de mujeres hay en un grupo de estudiantes. 
 	
 * @version 1.0
 * @date 05.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int  terminalInputNumberMan = 0; 		// Entrada de numero de hombres a traves del terminal
int terminalInputNumberWoman = 0;	 	// Entrada de numero de mujeres a traves del terminal
int total = 0;                           //total de personas

float porcentageMan = 0.0;				// porcentaje de hombres
float porcentageWoman = 0.0;		// porcentaje de mujeres
void CalculatePorcentageMan();
void CalculatePorcentageWoman();

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float PorcentageMan(int terminalInputNumberMan,int total);
float PorcentageWoman(int terminalInputNumberWoman,int total);
 
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the number of man: ";
	cin>>terminalInputNumberMan;
	cout<<"\tWrite the number of woman: ";
	cin>>terminalInputNumberWoman;
}
//=====================================================================================================

void Calculate(){
	porcentageMan = PorcentageMan(int terminalInputNumberMan,int total);
	porcentageWoman = PorcentageWoman(int terminalInputNumberWoman,int total);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe porcentage of man is : "<< porcentageMan<<"\r\n";
	cout<<"\tThe porcentage of woman is : "<<porcentageWoman<<"\r\n";
	
}
//=====================================================================================================

float PorcentageMan(int Man,int total){
	
	return Man*100/(total );	
}
//=====================================================================================================

float CalculatePorcentageWoman(int Woman,int total){
	
	return Woman*100/(total );
}


