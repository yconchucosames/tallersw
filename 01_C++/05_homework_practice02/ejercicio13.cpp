/**
 * @file CodeReviewExercise13.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 13:
  Un tonel es un recipiente, generalmente de madera, muy utilizado para almacenar y mejorar un vino. 
  La forma de un tonel es muy caracter�stica y es un cilindro en el que la parte central es m�s gruesa,
   es decir, tiene un di�metro mayor que los extremos. Escriba un programa que lea las medidas de un tonel y nos devuelva su
capacidad, teniendo en cuenta que el volumen (V) de un tonel viene dado por la siguiente f�rmula: V = ? l a2 donde:
 l    es la longitud del tonel, su altura. a = d/2 + 2/3(D/2 - d/2)
d   es el di�metro del tonel en sus extremos.
D  es el di�metro del tonel en el centro: D>d

 * @version 1.0
 * @date 20.01.2022
 * 
 */
 


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float volumen=0.0;           
 	float longitud=0.0;               
 	float diametro = 0;	
	float diametroM=0.0;
	float altura=0.0;
			

 	
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationVolumen(float longitud,float altura );
 	float CalculationAltura(float diametro,float diametroM  );

	
	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Ingrese la longitud del tonel :\r\n";
 	cin>>longitud;
 	cout<<"ingrese la altura del tonel :\r\n";
 	cin>>altura;
 	cout<<"ingrese el diametro de las bases :\r\n";
 	cin>>diametro;
 	cout<<"ingrese el diametro mayor : \r\n";
 	cin>>diametroM;
 	
	
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
	 volumen=CalculationVolumen( longitud,altura );
	 altura=CalculationAltura( diametro, diametroM  );
}
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"El volumen del tonel es   : "<<volumen<<"\r\n";
 
	 }
//=====================================================================================================

	float CalculationAltura(float diametro,float diametroM){
	
	
	return diametro/2 + diametroM/3-diametro/3;
	
	
}


//=====================================================================================================

	float CalculationVolumen(float longitud,float altura){
	
	return  M_PI*longitud*altura*altura;
}

