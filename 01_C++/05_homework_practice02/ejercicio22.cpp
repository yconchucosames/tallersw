/**
 * @file CodeReviewExercise22.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 22:
 Dado un tiempo expresado en HH:MM y otro tiempo en MM: SS, 
 dise�e un programa que calcule la suma de los tiempos y lo exprese en HH:MM:SS.
 * @version 1.0
 * @date 27.01.2022
 * 
 */
 


/*****************
 *  												INCLUDE
 *****************/
#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;


/*****************
 *  												GLOBAL VARIABLES
 *****************/
//Declaration e initialization of the variables
 	float horas1;            
	float minutos1;   
	float minutos2;              
	float segundos2;
	float tiempo_horas;
	float tiempo_minutos;
	float tiempo_segundos;
		

	
	
/*****************
 *  											FUNCTION DECLARATION
 *****************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationSumaH(float horas1,float minutos1,float minutos2,float segundos2 );
 	float CalculationSumaM(float horas1,float minutos1,float minutos2,float segundos2 );
 	float CalculationSumaS(float horas1,float minutos1,float minutos2,float segundos2 );
 	
 
 
 	

/*****************
 *  												MAIN
 *****************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /*****************
 *  												FUNCTION DEFINITION
 *****************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar tiempo en HH:MM :\r\n";
 	cout<<"ingresar horas :"<<endl;
	cin>>horas1;
 	cout<<"ingresar minutos :"<<endl;
 	cin>>minutos1;
 
 
 	cout<<"ingresar la cantidad de minutos :\r\n";
 	cin>>minutos2;
 	cout<<"ingresar la cantidad de segundos :\r\n";
 	cin>>segundos2;
 	
 	
 	

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	 tiempo_horas=CalculationSumaH(horas1,minutos1,minutos2,segundos2);
	 tiempo_minutos=CalculationSumaM(horas1,minutos1,minutos2,segundos2 );
	 tiempo_segundos=CalculationSumaS(horas1,minutos1,minutos2,segundos2 );
	
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"La suma de los tiempos en HH:MM:SS es     : "<<tiempo_horas<<"\r\n";
 	
 
	 }
//=====================================================================================================

		float CalculationSumaH(float horas1,float minutos1,float minutos2,float segundos2 ){
		
 	
			
			return horas1;
	}
	
//=====================================================================================================
	

