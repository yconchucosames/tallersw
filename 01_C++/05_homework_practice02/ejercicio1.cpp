/**
 * @file CodeReviewExercise01.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 01:
 Elabore un algoritmo que dados como datos  de entrada el radio y la altura de un cilindro calcular, 
 el �rea lateral y el volumen del cilindro.
       A = 2   radio*altura	V =   radio2*altura

 * @version 1.0
 * @date 27.01.2022
 * 
 */
 


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>



using namespace std;


/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float radio=0.0;            
	float altura=0.0;   
	float volumen=0.0;  
	float area=0.0;
	        
	float pi=3.14159265;             
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationArea(float radio,float altura );
 	float CalculationVolumen( float radio, float altura );
 
 
 	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar el radio del cilindro :\r\n";
 	cin>>radio;
 	cout<<"ingresar la altura del cilindro :\r\n";
 	cin>>altura;
    

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	area=CalculationArea( radio, altura );
	volumen=CalculationVolumen( radio, altura );
	
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"El area lateral y volumen del cilindro son  : "<<area<<" y "<<volumen<<"  respectivamente "<<endl;
 	
	 }
//=====================================================================================================

		float CalculationArea(float radio,float altura ){
		
		
			return  2*pi*radio*altura ;
	}
//===================================================================================================== 
 
 float CalculationVolumen( float radio, float altura ){
 	
 	
 	
 			return   pi*pow(radio,2)*altura ;
 }
	
