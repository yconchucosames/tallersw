/**
 * @file CodeReviewExercise05.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 05:
 		El costo de un autom�vil nuevo para un comprador es la suma total del costo del veh�culo,
		  del porcentaje de la ganancia del vendedor y de los impuestos locales o estatales aplicables (sobre el precio de venta). 
		  Suponer una ganancia del vendedor del 12% en todas las unidades y un impuesto del 6% 
		  y dise�ar un algoritmo para leer el costo total del autom�vil e imprimir el costo para el consumidor.

 * @version 1.0
 * @date 17.12.2021
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float  preciodeventaalpublico=0.0;            //precio de venta al publico
 	float  preciodecostodelvehiculo =0.0;               //precio de costo del vehiculo
 	
/***********************************************
 *  												FUNCTION DECLARATION
 ***********************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculationPreciodeVenta(float preciodeventa);




/***********************************************
 *  												MAIN
 ***********************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar precio de costo del vehiculo:\r\n";
 	cin>>preciodecostodelvehiculo;
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	preciodeventaalpublico =CalculationPreciodeVenta(preciodeventaalpublico); 
 	
 	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"El precio de venta al publico es:S/."<<preciodeventaalpublico<<"\r\n";
 
 	
	 }
//=====================================================================================================

float CalculationPreciodeVenta(float preciodeventaalpublico){
	return	100*preciodecostodelvehiculo/82;
	
}

//=====================================================================================================
