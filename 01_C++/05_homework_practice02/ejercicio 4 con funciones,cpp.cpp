/**
 * @file CodeReviewExercise04.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 04:
 		Un departamento de climatolog�a ha realizado recientemente su conversi�n al sistema m�trico. Dise�ar un algoritmo para realizar las siguientes conversiones:
a. Leer la temperatura dada en la escala Celsius e imprimir en su equivalente Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en mil�metros (25.5 mm = 1pulgada.


 * @version 1.0
 * @date 17.12.2021
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float  temperatureCelsius=0.0;            //entrada de la temperatura en celsius
 	float  waterInches =0.0;               //entrada de la cantidad de agua en pulgadas
 	float   temperatureFahrenheit = 0.0;			//salida de la temperatura en fahrenheit
 	float   waterMillimeters = 0.0;				//salida de la cantidad de agua en milimetros
 	
/***********************************************
 *  												FUNCTION DECLARATION
 ***********************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculationTemperatureFahrenheit(float celsius);
float CalculationwaterMillimeters(float inches);



/***********************************************
 *  												MAIN
 ***********************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar temperatura en celsius :\r\n";
 	cin>>temperatureCelsius;
 	cout<<"tingresar la cantidad de agua en pulgadas:\r\n";
 	cin>>waterInches;
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	temperatureFahrenheit =CalculationTemperatureFahrenheit(temperatureCelsius); 
 	waterMillimeters =CalculationwaterMillimeters(waterInches);
 	
}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"la temperatura en fahrenheit es:"<<temperatureFahrenheit<<"\r\n";
 	cout<<"\tla cantidad de agua en milimetros es :"<<waterMillimeters<<"\r\n";
 	
	 }
//=====================================================================================================

float CalculationTemperatureFahrenheit(float celsius){
	return	9.0*((float)temperatureCelsius)/5.0+32.0;
	
}
//=====================================================================================================

float CalculationWaterMillimeters(float inches){  
	 return	25.5*((float)waterInches);
	 
	 }
	 


//=====================================================================================================
