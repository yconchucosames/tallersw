/**
 * @file CodeReviewExercise11.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 11:
 		Hacer un algoritmo para ingresar una medida en metros, y que imprima esa medida expresada en
		  centímetros, pulgadas, pies y yardas. Los factores de conversión son los siguientes:
1 yarda = 3 pies
1 pie = 12 pulgadas
1 metro = 100 centímetros        12*2.54
1 pulgada = 2.54 centímetros

 * @version 1.0
 * @date 20.01.2022
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float metros=0.0;           
 	float centimetros=0.0;               
 	float pulgadas = 0;			
	float pies=0.0;	
	float yardas=0.0;
	
 	
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationCentimetros(float metros );
	float CalculationPulgadas(float centimetros,float metros );
	float CalculationPies(float metros,float centimetros,float pulgadas );
	float CalculationYardas(float metros ,float centimetros,float pulgadas,float pies);
	
	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Ingrese la medida en metros  :\r\n";
 	cin>>metros;
	
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
	 centimetros=CalculationCentimetros(metros);
	 pulgadas=CalculationPulgadas(centimetros,metros );
	 pies=CalculationPies(metros,centimetros,pulgadas );
	 yardas=CalculationYardas(metros,centimetros,pulgadas,pies);


}
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"a continuacion se mostrara la medida  en sus equivalentes  :centimetros,pulgadas,pies y yardas  \r\n";
 	cout<<"centimetros : "<<centimetros<<"\r\n";
 	cout<<"pulgadas  : "<<pulgadas<<"\r\n";
 	cout<<"pies :"<<pies<<"\r\n";
 	cout<<"yardas :"<<yardas;
 	
	 }
//=====================================================================================================

float CalculationCentimetros(float metros ){
	
	return  metros*100;
}
//=====================================================================================================

	float CalculationPulgadas(float centimetros,float metros ){
	
	return  2.54*pow(10,-2)*metros;
}

//=====================================================================================================

	float CalculationPies(float metros,float centimetros,float pulgadas ){
	
	return 2.54*pow(10,-2)*metros*12;
}

//=====================================================================================================

	float CalculationYardas(float  metros , float centimetros,float  pulgadas, float pies){
	
	return 2.54*pow(10,-2)*metros*12*3;
}


