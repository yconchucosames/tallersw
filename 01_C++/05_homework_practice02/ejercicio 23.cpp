/**
 * @file CodeReviewExercise23.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 23:
 En todo triangulo se cumple que cada lado es proporcional al seno del ángulo opuesto. 
 Esta ley se llama la ley de los senos Matemáticamente.

							a/"sen\?" =b/"sen\?" =c/"sen\?" 
Si se conocen los ángulos  "\?\,\?\,\?"  y el lado c. ¿Cuanto valen los otros dos lados? 

 * @version 1.0
 * @date 27.01.2022
 * 
 */
 


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>



using namespace std;


/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float lado_c=0.0;            
	float lado_a=0.0;   
	float lado_b=0.0;  
	float angulo_a=0.0;
	float angulo_b=0.0;
	float angulo_c=0.0;

	            
	float pi=3.14159265;             
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationLadoA(float lado_c,float angulo_a,float angulo_c );
 	float CalculationLadoB( float lado_c, float angulo_b,float  angulo_c );
 
 
 	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar el lado C del triangulo :\r\n";
 	cin>>lado_c;
 	cout<<"ingresar el angulo del vertice A :\r\n";
 	cin>>angulo_a;
    cout<<"ingresar el angulo del vertice B :\r\n";
 	cin>>angulo_b;
 	cout<<"ingresar el angulo del vertice C:\r\n";
 	cin>>angulo_c;

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	lado_a=CalculationLadoA( lado_c, angulo_a, angulo_c );
	lado_b=CalculationLadoB( lado_c, angulo_b, angulo_c );
	
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"los valores del lado a y b son : "<<lado_a<<" y "<<lado_b<<"  respectivamente "<<endl;
 	
	 }
//=====================================================================================================

	float CalculationLadoA(float lado_c,float angulo_a,float angulo_c ){
		
		
			
			return  sin(angulo_a*pi/180)*lado_c/sin(angulo_c*pi/180) ;
	}
//===================================================================================================== 
 
 float CalculationLadoB( float lado_c, float angulo_b,float  angulo_c ){
 	
 	
 	
 	return    sin(angulo_b*pi/180)*lado_c/sin(angulo_c*pi/180);
 }
	
