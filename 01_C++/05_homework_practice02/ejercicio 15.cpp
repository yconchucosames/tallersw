/**
 * @file CodeReviewExercise15.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 15:
 Escriba un programa en C para calcular el valor de la pendiente de una l�nea que conecta dos puntos (x1,y1) y (x2,y2).
  La pendiente est� dada por la ecuaci�n (y2-y1)/(x2-x1).
   Haga que el programa tambi�n calcule el punto medio de la l�nea que une los dos puntos, el cual viene dado por (x1+x2)/2,(y1+y2)/2.
    �Cu�l es el resultado que devuelve el programa para los puntos (3,7) y (8,12)? 
 * @version 1.0
 * @date 24.01.2022
 * 
 */
 


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float  puntox1=0.0;            
	float  puntoy1=0.0;
	float a=0.0;
	float b=0.0;
	float puntox2=0.0;          
 	float puntoy2=0.0;               
 	float pendiente=0.0;	
	float Xm=0.0;             //abcisa del punto medio
	float Ym=0.0;             //ordenada del punto medio
			

 	
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationPendiente(float puntox1,float puntoy1,float puntox2,float puntoy2 );
 	float CalculationPuntoXm(float puntox1,float puntoy1,float puntox2,float puntoy2);
 	float CalculationPuntoYm(float puntox1,float puntoy1,float puntox2,float puntoy2);
 	
 	

	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Ingrese x1 :\r\n";
 	cin>>puntox1;
 	cout<<"ingrese y1:\r\n";
 	cin>>puntoy1;
 	cout<<"ingrese x2:\r\n";
 	cin>>puntox2;
 	cout<<"ingrese y2: \r\n";
 	cin>>puntoy2;
 	

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
	pendiente=CalculationPendiente( puntox1, puntoy1, puntox2, puntoy2 );
 	Xm=CalculationPuntoXm( puntox1, puntoy1, puntox2, puntoy2);
 	Ym=CalculationPuntoYm( puntox1, puntoy1, puntox2, puntoy2); 
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"La pendiente de la recta es  : "<<pendiente<<"\r\n";
 	cout<<"El punto medio de la recta es : "<<"("<<Xm<<","<<Ym<<")"<<"\r\n";
 	
 
	 }
//=====================================================================================================

	float CalculationPendiente(float puntox1,float puntoy1,float puntox2,float puntoy2 ){
	
	
	return (puntoy2-puntoy1)/(puntox2-puntox1) ;
	
	
}


//=====================================================================================================

	float CalculationPuntoXm(float puntox1,float puntoy1,float puntox2,float puntoy2){
		
		
		return  (puntox1+puntox2)/2;

	}
//=====================================================================================================	


float CalculationPuntoYm(float puntox1,float puntoy1,float puntox2,float puntoy2){
		
		
		return  (puntoy1+puntoy2)/2;

}
