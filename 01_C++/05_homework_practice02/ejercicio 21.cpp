/**
 * @file CodeReviewExercise21.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 21:
 Calcular la velocidad de un auto en Km/h, ingresando la distancia recorrida en metros y el tiempo en minutos
 * @version 1.0
 * @date 27.01.2022
 * 
 */
 


/*****************
 *  												INCLUDE
 *****************/
#include <iostream>

using namespace std;


/*****************
 *  												GLOBAL VARIABLES
 *****************/
//Declaration e initialization of the variables
 	float distancia;            
	float tiempo;   
	float velocidad_auto;              

/*****************
 *  											FUNCTION DECLARATION
 *****************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationVelocidad(float distancia,float tiempo);
 	
/*****************
 *  												MAIN
 *****************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /*****************
 *  												FUNCTION DEFINITION
 *****************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar la distancia en metros :\r\n";
 	cin>>distancia;
 	cout<<"ingresar el tiempo en minutos :\r\n";
 	cin>>tiempo;


}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	 velocidad_auto=CalculationVelocidad( distancia, tiempo);
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"la velocidad en kilometros por hora es  : "<<velocidad_auto<<"\r\n";

	 }
//=====================================================================================================

	CalculationVelocidad(float distancia,float tiempo){
		
		
			
			return (distancia/1000)/(tiempo/60) ;
	}
	

