/**
 * @file CodeReviewExercise20.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 20:
 Ingresar los lados de un triangulo y el �ngulo que forman, e imprima el  valor del tercer lado, 
  los otros dos �ngulos y el �rea del tri�ngulo.
 * @version 1.0
 * @date 29.01.2022
 * 
 */
 


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <cmath>
#include <math.h>



using namespace std;


/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float lado_a=0.0;            
	float lado_b=0.0;   
	float lado_c=0.0; 
	float angulo_a=0.0;
	float angulo_b=0.0;
	float area=0.0;
	float angulo_c=0.0;           
	float pi=3.14159265;
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationArea(float lado_a,float lado_b,float angulo_c);
 	float CalculationLadoC(float lado_a,float lado_b,float angulo_c);
 	float CalculationAnguloA(float lado_a,float lado_b,float lado_c);
 	float CalculationAnguloB(float lado_a,float lado_b,float lado_c);
   
 	
 
 
 	

/***********************************************
 *  												MAIN
 ***********************************************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar el lado a :\r\n";
 	cin>>lado_a;
 	cout<<"ingresar el lado b :\r\n";
 	cin>>lado_b;
 	cout<<"ingresar angulo c :\r\n";
 	cin>>angulo_c;
 	

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	 area=CalculationArea( lado_a, lado_b, angulo_c);
 	 lado_c=CalculationLadoC( lado_a, lado_b,angulo_c);
 	 angulo_a=CalculationAnguloA( lado_a, lado_b, lado_c);
 	 angulo_b=CalculationAnguloB( lado_a, lado_b, lado_c);
	
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"el area del triangulo es    : "<<area<<"\r\n";
 	cout<<"el lado C es : "<<lado_c<<"\r\n";
 	cout<<"el angulo A es : "<<angulo_a<<"\r\n";
 	cout<<"el angulo B  : "<<angulo_b<<"\r\n";
 	
 	
	 }
//=====================================================================================================

			float CalculationArea(float lado_a,float lado_b,float angulo_c ){
		
		
			
			return  (lado_a*lado_b*sin(angulo_c*pi/180))/2;
	}
 //=====================================================================================================

	
		float CalculationLadoC(float lado_a,float lado_b,float angulo_c){
			
			
			return sqrt(pow(lado_a,2)+pow(lado_b,2)-lado_b*lado_a*cos(angulo_c*pi/180));
		}

//=====================================================================================================

	

	float CalculationAnguloA(float lado_a,float lado_b,float lado_c){
		
		
		return acos((pow(lado_b,2)+pow(lado_c,2)-pow(lado_a,2))/2*lado_b*lado_c);
	}
	
//=====================================================================================================

		
		float CalculationAnguloB(float lado_a,float lado_b,float lado_c){
			
			
			return  acos((pow(lado_a,2)+pow(lado_c,2)-pow(lado_b,2))/2*lado_a*lado_c);
		}
