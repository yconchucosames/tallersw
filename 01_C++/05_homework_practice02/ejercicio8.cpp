/**
 * @file CodeReviewExercise08.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 08:
 		Calcular el monto final, dados como datos el Capital Inicial, el  tipo de Inter�s, el numero de periodos por a�o, 
		 y el numero de a�os de  la inversi�n. El c�lculo del capital final se basa en la formula del inter�s compuesto.
				M = C(1+i/N)
	Donde:
	M = Capital final o Monto,	C = Capital Inicial,	i = Tipo de inter�s nominal
	N = Numero de periodos por a�o,		T = Numero de a�os
- Si un cliente deposita al Banco la cantidad de $10,000 a inter�s compuesto con una tasa del 8% anual.  
�Cual ser� el monto que recaude despu�s de 9 a�os? 
 - �Cuanto debe cobrar el cliente dentro de 3 a�os si deposita  $ 100,000. al 9% anual  y capitaliz�ndose los intereses bimestralmente?   


 * @version 1.0
 * @date 20.01.2022
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float   capitalinicial=0.0;            
 	float   tipodeinteres=0.0;               
 	int   periodos = 0;			
 	int   aniosdeinversion = 0;
	float capitalfinal=0.0;	
 	
	
/***********************************************
 *  												FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationCapitalFinal(float capitalinicial,float tipodeinteres,float periodos);



/***********************************************
 *  												MAIN
 ***********************************************/

 int main(){
 	
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar capital incial :\r\n";
 	cin>>capitalinicial;
 	cout<<"tingresar tipo de interes:\r\n";
 	cin>>tipodeinteres;
 	cout<<"tingresar numero de periodos :\r\n";
 	cin>>periodos;
 	cout<<"ta�os de inversion :\r\n";
 	cin>>aniosdeinversion;
 	
 	
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
	capitalfinal =CalculationCapitalFinal(capitalinicial,tipodeinteres, periodos);

}
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"El capital final es  :"<<capitalfinal<<"\r\n";
 	
	 }
//=====================================================================================================

float CalculationCapitalFinal(float capitalinicial,float tipodeinteres,float periodos){
	return      capitalinicial*pow(1+tipodeinteres/100,periodos);
	
}
//=====================================================================================================


