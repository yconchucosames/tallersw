/**
 * @file CodeReviewExercise09.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 09:
 		�Cu�l es el capital que debe colocarse a inter�s  compuesto del 8%  anual 
		 para que despu�s de 20 a�os produzca un monto de $ 500,000. ? 
 * @version 1.0
 * @date 20.01.2022
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	float   capitalinicial=0.0;            
 	float   tasainteres=0.0;               
 	int   periodos = 0;			
	float capitalfinal=0.0;	
 	
	
/***********************************************
 *  											FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	float CalculationCapitalInicial(float capitalfinal,float tasainteres,float periodos);



/***********************************************
 *  												MAIN
 ***********************************************/

 int main(){
 	
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar capital final :\r\n";
 	cin>>capitalfinal;
 	cout<<"tingresar tasa de interes:\r\n";
 	cin>>tasainteres;
 	cout<<"tingresar numero de periodos :\r\n";
 	cin>>periodos;
 	
 	
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
	capitalinicial =CalculationCapitalInicial(capitalfinal,tasainteres, periodos);

}
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"El capital inicial es  :"<<capitalinicial<<"\r\n";
 	
	 }
//=====================================================================================================

float CalculationCapitalInicial(float capitalfinal,float tasainteres,float periodos){
	return      capitalfinal/pow(1+tasainteres/100,periodos);
	
}
//=====================================================================================================


