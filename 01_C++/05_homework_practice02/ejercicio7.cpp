/**
 * @file CodeReviewExercise07.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 07:
 		Calcular la fuerza de atracci�n entre dos masas, separadas por una distancia, mediante la siguiente f�rmula:
F = G*masa1*masa2 / distancia2
Donde G es la constante de gravitaci�n universal: G = 6.673 * 10-8 cm3/g.seg2


 * @version 1.0
 * @date 17.01.2021
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	double   masa1=0.0;            
 	double   masa2=0.0;               
 	double   distancia = 0.0;			
 	double   fuerzadegravedad = 0.0;	
 	double   G=6.673*pow(10,-8);		//constante de gravitacion universal
 	
	
/***********************************************
 *  												FUNCTION DECLARATION
 ***********************************************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
 	double CalculationFuerzadegravedad(double masa1,double masa2,double distancia);



/***********************************************
 *  												MAIN
 ***********************************************/

 int main(){
 	
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar masa 1 :\r\n";
 	cin>>masa1;
 	cout<<"tingresar masa2:\r\n";
 	cin>>masa2;
 	cout<<"ingresar distancia :\r\n";
 	cin>>distancia;
 	
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
	fuerzadegravedad =CalculationFuerzadegravedad(masa1,masa2,distancia); 
 	
}
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"la fuera de atraccion entre las 2 masas es :"<<fuerzadegravedad<<"\r\n";
 	
	 }
//=====================================================================================================

double CalculationFuerzadegravedad(double masa1,double masa2,double distancia){
	return	G*masa1*masa2/pow(distancia,2);
	
}
//=====================================================================================================


