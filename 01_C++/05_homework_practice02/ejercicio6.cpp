/**
 * @file CodeReviewExercise06.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 06:
 		Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos.

 * @version 1.0
 * @date 15.01.2022
 * 
 */


/***********************************************
 *  												INCLUDE
 ***********************************************/
#include <iostream>
#include <math.h>

using namespace std;
/***********************************************
 *  												GLOBAL VARIABLES
 ***********************************************/
//Declaration e initialization of the variables
 	int  totaldesegundos=0;            //total de segundos a desglosar
 	int  dias=0.0;               //precio de costo del vehiculo
 	int horas=0.0;
 	int minutos=0.0;
 	int segundos=0.0;
/***********************************************
 *  												FUNCTION DECLARATION
 ***********************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int CalculationDias(int totaldesegundos);
int CalculationHoras(int totaldesegundos);
int CalculationMinutos(int totaldesegundos);
int CalculationSegundos(int totaldesegundos);




/***********************************************
 *  												MAIN
 ***********************************************/

 int main(){
	
	Run();
 	return 0;
 	
 }
 /***********************************************
 *  												FUNCTION DEFINITION
 ***********************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar el total de segundos :\r\n";
 	cin>>totaldesegundos;
 	
 	
}
//=====================================================================================================
	//Operation of  the variables
void Calculate(){
	dias =CalculationDias(totaldesegundos);
	horas=CalculationHoras(totaldesegundos);
	minutos=CalculationMinutos(totaldesegundos);
	segundos=CalculationSegundos(totaldesegundos);
 	
 	}
//=====================================================================================================
	//Show the calculation of the terminal
void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"Dias: "<<dias<<"\r\n";
 	cout<<"horas: "<<horas<<endl;
 	cout<<"minutos: "<<minutos<<endl;
 	cout<<"segundos: "<<segundos<<endl;
 
 	
	 }
//=====================================================================================================

	int CalculationDias(int totaldesegundos){
		
	return	totaldesegundos/86400;
	
	}
	int CalculationHoras(int totaldesegundos){
		
		totaldesegundos=totaldesegundos%86400;
		
		return  (totaldesegundos)/3600;
	
		
	}
	
	int CalculationMinutos(int totaldesegundos){
		
		totaldesegundos=totaldesegundos%3600;
		
		return  	totaldesegundos/60;
		
		
	}
	
	int CalculationSegundos(int totaldesegundos){
		
		
		return 	totaldesegundos%60;
		
		
	}

//=====================================================================================================
