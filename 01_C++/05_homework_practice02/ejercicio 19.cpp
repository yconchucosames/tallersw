/**
 * @file CodeReviewExercise19.cpp
 * @author yhen conchucos (yconchucosames@gmail.com)
 * @brief exercise 19:
 Dados como datos las coordenadas de los tres puntos P1, P2, P3 que corresponden a los v�rtices de un triangulo, 
 calcule su per�metro y �rea.
 * @version 1.0
 * @date 27.01.2022
 * 
 */
 


/*****************
 *  												INCLUDE
 *****************/
#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;


/*****************
 *  												GLOBAL VARIABLES
 *****************/
//Declaration e initialization of the variables
 	float abcisa1=0.0;            
	float ordenada1=0.0;   
	float abcisa2=0.0;              
	float ordenada2=0.0;
	float abcisa3=0.0;            
	float ordenada3=0.0;             
	float area=0.0;
	float perimetro=0.0;

	
	
/*****************
 *  											FUNCTION DECLARATION
 *****************/
	void Run();
	void CollectData();
	void Calculate();
	void ShowResults();
	void
 	float CalculationArea(float abcisa1,float ordenada1,float abcisa2,float ordenada2,float abcisa3,float ordenada3 );
 	float CalculationPerimetro(float abcisa1,float ordenada1,float abcisa2,float ordenada2,float abcisa3,float ordenada3 );
 
 
 	

/*****************
 *  												MAIN
 *****************/

 	int  main(){
 	
	
	Run();
	
 	return 0;
 	
 }
 /*****************
 *  												FUNCTION DEFINITION
 *****************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

	//Data entry in the terminal
void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"ingresar punto en x para P1 :\r\n";
 	cin>>abcisa1;
 	cout<<"ingresar punto en y para P1 :\r\n";
 	cin>>ordenada1;
 	cout<<"ingresar punto en x para P2 :\r\n";
 	cin>>abcisa2;
 	cout<<"ingresar punto en y para P2 :\r\n";
 	cin>>ordenada2;
 	cout<<"ingresar punto en x para P3 :\r\n";
 	cin>>abcisa3;
 	cout<<"ingresar punto en y para P3 :\r\n";
 	cin>>ordenada3;
 	
 	
 	

}
//=====================================================================================================
	//Operation of  the variables
	void Calculate(){
		
	 area=CalculationArea( abcisa1, ordenada1, abcisa2, ordenada2, abcisa3, ordenada3);
	perimetro=CalculationPerimetro( abcisa1, ordenada1, abcisa2, ordenada2, abcisa3, ordenada3);
	  }
//=====================================================================================================
	//Show the calculation of the terminal
	void ShowResults(){
		
 	cout<<"============Show result============\r\n";
 	cout<<"el area del triangulo es    : "<<area<<"\r\n";
 	cout<<"el perimetro del triangulo es    : "<<perimetro<<"\r\n";
 
	 }
//=====================================================================================================

	float CalculationArea(float abcisa1,float ordenada1,float abcisa2,float ordenada2,float abcisa3,float ordenada3  ){
		
		
			
			return (abs(abcisa1*(ordenada2-ordenada3)+abcisa2*(ordenada3-ordenada1)+abcisa3*(ordenada1-ordenada2)))/2 ;
	}
	
//=====================================================================================================
	
	float CalculationPerimetro(float abcisa1,float ordenada1,float abcisa2,float ordenada2,float abcisa3,float ordenada3 ){
		
		
		
		return sqrt(pow(abcisa2-abcisa1,2)+pow(ordenada2-ordenada1,2))+sqrt(pow(abcisa3-abcisa1,2)+pow(ordenada3-ordenada1,2))+sqrt(pow(abcisa3-abcisa2,2)+pow(ordenada3-ordenada2,2));
	}
